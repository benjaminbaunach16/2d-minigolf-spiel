const startAngle = 0;
const endAngle = Math.PI * 2;

function distance(x1, y1, x2, y2) {
    return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
}


class ball {

    constructor(ctx, x, y, radius, dx = 0, dy = 0, vector, fillStyle = "#fff", strokeStyle = "#000", lineWidth = 1) {
        this.ctx = ctx;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.fillStyle = fillStyle;
        this.lineWidth = lineWidth;
        this.strokeStyle = strokeStyle;
        this.dx = dx;
        this.dy = dy;
        this.vector = vector;
        this.distanceBall = false;


    }


    //toch start 
    ballTouchEvent_start(touchX_start, touchY_start) {


        this.distanceBall = distance(this.x, this.y, touchX_start, touchY_start) < this.radius;
        //prüfen ob ball berührt wurde
        if (!this.distanceBall) return;


        //touch start werte in vector speichern 
        this.vector.x_start = this.x;
        this.vector.y_start = this.y;




    }

    // touch move
    ballTouchEvent_move(touchX_move, touchY_move, newX, newY, shootButton) {



        if (!this.distanceBall) return;

        // geschwindigkeits Faktor einbauen 
        let distance = Math.sqrt((touchX_move - this.x) * (touchX_move - this.x) + (touchY_move - this.y) * (touchY_move - this.y));



        if (distance >= 350) {
            newX = newX * 0.4;
            newY = newY * 0.4;
        }


        //weiterer Faktor 
        let faktor = Math.sqrt((newX - 0) * (newX - 0) + (newY - 0) * (newY - 0)) * 0.0015;
        if (faktor >= 0.2) faktor = 0.2;
        this.dx = faktor * newX;
        this.dy = faktor * newY;

        shootButton.ballTouched = true;

        this.vector.x_end = touchX_move;
        this.vector.y_end = touchY_move;
    }


    //Touch ende touchX_end,to
    ballTouchEvent_end(shootButton) {

        this.distanceBall = false;

        //man kann nur schießen, wenn der ball berührt wird! --> Bei false !! 
        shootButton.ballTouched = false;


    }


    //Ball bewegung 
    ballMove(gameField, shootButton, hinderniss) {

        this.x += this.dx;
        this.y += this.dy;

        this.ballEdgetouch(gameField, hinderniss);

        this.dx *= 0.99;
        this.dy *= 0.99;

        this.handleReibungBall(shootButton);

    }

    //Ball prallt am rand ab 
    ballEdgetouch(gameField, hinderniss) {
        if (this.x + this.radius > gameField.x + gameField.w || this.x - this.radius < gameField.x) {


            this.dx = -this.dx;


        }
        if (this.y + this.radius > gameField.y + gameField.h || this.y - this.radius < gameField.y) {


            this.dy = -this.dy;


        }


        //Kollision mit hindernis 
        var circleTop = this.y - this.radius;
        var circleBottom = this.y + this.radius;
        var circleLeft = this.x - this.radius;
        var circleRight = this.x + this.radius;

        // For uniformity with circle
        var rectTop = hinderniss.y;
        var rectBottom = hinderniss.y + hinderniss.h;
        var rectLeft = hinderniss.x
        var rectRight = hinderniss.x + hinderniss.w;

        // Circle penetration on the div's left and right walls
        if (((circleRight > rectLeft && circleLeft < rectLeft) ||
            (circleLeft < rectRight && circleRight > rectRight)) &&
            circleTop < rectBottom && circleBottom > rectTop) {

            this.dx = -this.dx;
        }

        // Circle penetration on the div's top and bottom walls
        if (((circleBottom > rectTop && circleTop < rectTop) ||
            (circleTop < rectBottom && circleBottom > rectBottom)) &&
            circleLeft < rectRight && circleRight > rectLeft) {

            this.dy = -this.dy;
        }

    }


    //Ball wird langsamer
    handleReibungBall(shootButton) {
        const endWert = 0.04;


        if (Math.abs(this.dx) < endWert) {
            this.dx = 0
            shootButton.shoot = false;
        }

        if (Math.abs(this.dy) < endWert) {
            this.dy = 0;
            shootButton.shoot = false;
        }

    }

    //Ball Zeichnen 
    drawBall() {


        this.ctx.fillStyle = this.fillStyle;
        this.ctx.strokeStyle = this.strokeStyle;
        this.ctx.lineWidth = this.lineWidth;


        this.ctx.beginPath();
        this.ctx.arc(this.x, this.y, this.radius, startAngle, endAngle, true);
        this.ctx.fill();
        this.ctx.stroke();

    }

    //Schießrichtung zeichnen

    ballReturnDistance() {
        return this.distanceBall;
    }

    drawBallline(ctx) {


        ctx.lineWidth = 2;
        ctx.lineCap = "round";
        ctx.strokeStyle = "rgba(255,255,255, 0.5)"
        ctx.beginPath();
        ctx.moveTo(this.x, this.y);
        ctx.lineTo(this.vector.x_end, this.vector.y_end);


        ctx.stroke();


        if (!this.distanceBall) {
            //Hier muss der Strich gelöscht werden
        }
    }


}


class button {
    constructor(ctx, x, y, width, height, vector, fillStyle = "purple", strokeStyle = "black", lineWidth = 5) {
        this.ctx = ctx;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.fillStyle = fillStyle;
        this.lineWidth = lineWidth;
        this.strokeStyle = strokeStyle;
        this.vector = vector;
        this.ballTouched = false;
        this.shoot = false;
        this.dx = 0;
        this.dy = 0;
    }



    drawButton() {
        this.ctx.lineWidth = this.lineWidth;
        this.ctx.strokeStyle = this.strokeStyle;
        this.ctx.fillStyle = this.fillStyle;

        this.ctx.resetTransform();
        this.ctx.fillRect(this.x, this.y, this.width, this.height);
        this.ctx.strokeRect(this.x, this.y, this.width, this.height);
        this.ctx.fillStyle = "black";
        this.ctx.font = "50px Arial";
        this.ctx.fillText("SHOOT", this.x + 10, this.y + 65);

    }




    buttonTouchEvent_start(touchX_start, touchY_start, strokeScoore) {
        if (touchX_start > this.x && touchX_start < this.x + this.width && touchY_start > this.y && touchY_start < this.y + this.height) {

            if (!this.ballTouched) return;
            this.shoot = true;
            this.fillStyle = "red";
            strokeScoore.stroke +=  1;

            //schuss wird aufgeladen

        }

    }

    buttonTouchEvent_move(touchX_move, touchY_move) {
        if (touchX_move > this.x && touchX_move < this.x + this.width && touchY_move > this.y && touchY_move < this.y + this.height) {


            // Strich gezeichnet?? 

            // Stich muss voller werden

        }

    }

    buttonTouchEvent_end(touchX_end, touchY_end) {
        if (this.fillStyle == "red") this.fillStyle = "purple";
    }


    shootBall(gameField, gameBall, shootBall, hinderniss) {

        if (!this.shoot) return;
        gameBall.ballMove(gameField, shootBall, hinderniss);
        
    }


}


class field {

    constructor(ctx, x, y, w, h, fillStyle = "#fff", strokeStyle = "#000", lineWidth = 10) {
        this.ctx = ctx;

        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.fillStyle = fillStyle;
        this.lineWidth = lineWidth;
        this.strokeStyle = strokeStyle;
    }

    drawField() {
        this.ctx.lineWidth = this.lineWidth;
        this.ctx.strokeStyle = this.strokeStyle;
        this.ctx.fillStyle = this.fillStyle;



        this.ctx.fillRect(this.x, this.y, this.w, this.h);
        this.ctx.strokeRect(this.x, this.y, this.w, this.h);


    }



}

class hole {
    constructor(ctx, x, y, radius, fillStyle, strokeStyle, lineWidth) {
        this.ctx = ctx;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.fillStyle = fillStyle;
        this.lineWidth = lineWidth;
        this.strokeStyle = strokeStyle;
    }


    ballHoleCollision(gameBall, message) {

        if ((Math.abs(gameBall.x - this.x) < (gameBall.radius + this.radius)) && (Math.abs(gameBall.y - this.y) < (gameBall.radius + this.radius))) {

            gameBall.dx = 0;
            gameBall.dy = 0;
            message.ballTouchHole = true;


        }

    }

    drawHole() {

        this.ctx.fillStyle = this.fillStyle;
        this.ctx.strokeStyle = this.strokeStyle;
        this.ctx.lineWidth = this.lineWidth;


        this.ctx.beginPath();
        this.ctx.arc(this.x, this.y, this.radius, startAngle, endAngle, true);
        this.ctx.fill();
        this.ctx.stroke();
    }
}


class hinderniss {
    constructor(ctx, x, y, w, h, fillStyle = "black", strokeStyle = "#fff", lineWidth = 1) {

        this.ctx = ctx;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.fillStyle = fillStyle;
        this.lineWidth = lineWidth;
        this.strokeStyle = strokeStyle;
    }

    drawHinderniss() {
        this.ctx.lineWidth = this.lineWidth;
        this.ctx.strokeStyle = this.strokeStyle;
        this.ctx.fillStyle = this.fillStyle;



        this.ctx.fillRect(this.x, this.y, this.w, this.h);
        this.ctx.strokeRect(this.x, this.y, this.w, this.h);
    }


}

class message {
    constructor(ctx, x, y, width, height, fillStyle, strokeStyle, lineWidth) {
        this.ctx = ctx;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.fillStyle = fillStyle;
        this.lineWidth = lineWidth;
        this.strokeStyle = strokeStyle;
        this.ballTouchHole = false;

    }

    drawMessage(strokeScoore) {

        if (!this.ballTouchHole) return;
        this.ctx.lineWidth = this.lineWidth;
        this.ctx.strokeStyle = this.strokeStyle;
        this.ctx.fillStyle = this.fillStyle;

        this.ctx.resetTransform();
        this.ctx.fillRect(this.x, this.y, this.width, this.height);
        this.ctx.strokeRect(this.x, this.y, this.width, this.height);
        this.ctx.fillStyle = "black";
        this.ctx.font = "60px Arial";
        this.ctx.fillText("HERZLICHEN GLÜCKWUNSCH,", this.x + 10, this.y + 85);
        this.ctx.font = "55px Arial";
        this.ctx.fillText("DU HAST DAS LEVEL GESCHAFFT!", this.x + 10, this.y + 160);
        this.ctx.fillText("Schläge benötigt: " + strokeScoore.stroke, this.x + 250, this.y + 250);


    }
}


class strokeScoore {
    constructor(ctx, x, y, w, h, fillStyle = "purple", strokeStyle = "#000", lineWidth = 5) {
        this.ctx = ctx;
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
        this.fillStyle = fillStyle;
        this.lineWidth = lineWidth;
        this.strokeStyle = strokeStyle;
        this.stroke = 0; 

    }


    drawStrokeScore() {
        this.ctx.lineWidth = this.lineWidth;
        this.ctx.strokeStyle = this.strokeStyle;
        this.ctx.fillStyle = this.fillStyle;
        this.ctx.t

        this.ctx.resetTransform();
        this.ctx.fillRect(this.x, this.y, this.width, this.height);
        this.ctx.strokeRect(this.x, this.y, this.width, this.height);
        this.ctx.fillStyle = "black";
        this.ctx.font = "50px Arial";
        this.ctx.fillText("Stroke: " + this.stroke, this.x + 20 , this.y + 65);
    }


}



//button zum Neustarten 
class restartButton {
    constructor(ctx, x, y, width, height, fillStyle, strokeStyle, lineWidth) {
        this.ctx = ctx;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.fillStyle = fillStyle;
        this.lineWidth = lineWidth;
        this.strokeStyle = strokeStyle;
          }



    drawRestartButton() {
        this.ctx.lineWidth = this.lineWidth;
        this.ctx.strokeStyle = this.strokeStyle;
        this.ctx.fillStyle = this.fillStyle;

        this.ctx.resetTransform();
        this.ctx.fillRect(this.x, this.y, this.width, this.height);
        this.ctx.strokeRect(this.x, this.y, this.width, this.height);
        this.ctx.fillStyle = "black";
        this.ctx.font = "50px Arial";
        this.ctx.fillText("RESTART", this.x + 10, this.y + 65);

    }


    restartButtonTouchEvent_start(touchX_start, touchY_start) {
        if (touchX_start > this.x && touchX_start < this.x + this.width && touchY_start > this.y && touchY_start < this.y + this.height) {

            location.reload();
        }
    }
}


export { ball, field, button, hole, hinderniss, message, strokeScoore, restartButton };
