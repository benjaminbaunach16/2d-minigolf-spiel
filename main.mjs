
import {circle} from "./lib.mjs";
import {lib} from "./libClass.mjs";
import {ball, field, button, hole, hinderniss,message, strokeScoore, restartButton} from "./GameLib.mjs";

function distance(x1, y1, x2, y2) {
    return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
}


//scale faktor einbauen 


const contextWidth = 900; 
const contextHeight = 200; 



let scale = 1; 
const FILLSCALE = false;

(function Init() {
    let cnv = document.getElementById("cnv");
    let ctx= cnv.getContext("2d");
    ctx.imageSmoothingEnabled = true;


   let fingers = {};

   function setFingers(touches) {
       for (let t of touches) {    
           
           fingers[t.identifier] = {
               x: t.pageX,
               y: t.pageY,
           };
       }
   }

   function moveFingers(touches) {
       for (let t of touches) {
           fingers[t.identifier] = {
               x: t.pageX,
               y: t.pageY,
           };
       }
   }

   function rmFingers(touches) {
       for (let t of touches) {
           delete fingers[t.identifier];
       }

       
   }


    let gameBall = new ball(ctx, 150, 150, 15, 0, 0, fingers);
    let gameField = new field(ctx, 50 , 50 , contextWidth, contextHeight, "green");
    let gameHole = new hole(ctx, 800, 150, 10, "black", "white", 1);
    let shootButton = new button(ctx, 750, 280, 200, 100, fingers);
    let gameStrokeScore= new strokeScoore(ctx, 400, 280, 250, 100);
    let gameHindernis = new hinderniss(ctx, 400, 100, 100, 100);
    let gameRestartButton = new restartButton(ctx, 50, 280, 250, 100, "purple", "black", 5);
    let gameMessage = new message(ctx, 0, 0, 1000, 270, "purple", "black", 5);
    


    function resize() {
        cnv.width = window.innerWidth;
        cnv.height = window.innerHeight;
    }

    addEventListener("resize", resize);


    //Touchevents
   
    cnv.addEventListener("touchstart", (evt) => {
       
        for(let e of evt.changedTouches){
      

        const x = e.clientX;
        const y = e.clientY;     

        gameBall.ballTouchEvent_start(x,y);
        shootButton.buttonTouchEvent_start(x, y, gameStrokeScore);
        gameRestartButton.restartButtonTouchEvent_start(x, y);
                    
        evt.preventDefault();
        setFingers(evt.changedTouches);
       }
    }, true);

    
    cnv.addEventListener("touchmove", (evt) => {

        for(let e of evt.changedTouches){
      

            const x = e.clientX;
            const y = e.clientY;
    

        let newX = x - gameBall.x;
        let newY = y - gameBall.y;    

       

        gameBall.ballTouchEvent_move(x,y, newX ,newY, shootButton);
        shootButton.buttonTouchEvent_move(newX, newY);
   
        
        evt.preventDefault();
        moveFingers(evt.changedTouches);
        }
    }, true);



    cnv.addEventListener("touchend", (evt) => {
        for(let e of evt.changedTouches){
            const x = e.clientX;
            const y = e.clientY;
    

        gameBall.ballTouchEvent_end(shootButton);
        shootButton.buttonTouchEvent_end(x,y)


        

        
        evt.preventDefault();
        rmFingers(evt.changedTouches);
        }

    }, true);






    //BallBewegung
    function loop(){

        shootButton.shootBall(gameField, gameBall,shootButton, gameHindernis);

    }   



    function draw(){
     

        ctx.resetTransform();
        ctx.clearRect(0,0, cnv.width, cnv.height);

  
        //field
        gameField.drawField();
        ctx.resetTransform();    


        // Ball
        gameBall.drawBall();
        
        ctx.resetTransform();
      
        //Ball line
        gameBall.drawBallline(ctx);
        ctx.resetTransform();     
        
        //button
        shootButton.drawButton();
        ctx.resetTransform();

        //Ziel
        gameHole.drawHole();
        ctx.resetTransform();

        //hinderniss
        gameHindernis.drawHinderniss();
        ctx.resetTransform();

        //Restart Button
        gameRestartButton.drawRestartButton();
        ctx.resetTransform();

        gameStrokeScore.drawStrokeScore();
        ctx.resetTransform(); 
        

        //Kollisons erkennung
        gameHole.ballHoleCollision(gameBall, gameMessage);
        ctx.resetTransform();
        
        gameMessage.drawMessage(gameStrokeScore); 



    
        for (let f in fingers) {
            circle(ctx, fingers[f].x, fingers[f].y, 5, "#f00");
        }
        ctx.resetTransform();
       
       loop();
        window.requestAnimationFrame(draw);
        
    }

   
   
    resize();
    draw(); 
}())

